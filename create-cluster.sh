####################################################################
#
#  Init GKE cluster and simple app with LetsEnctypt ingress
#
#
####################################################################

EMAIL=pavmikulka@gmail.com
CLUSTERNAME=cluster-test-1
PROJECT=pm-test-01
#PROJECT=cognito-1504769605594

#############################
#set -e
set -x


#gcloud auth login
gcloud auth set account $EMAIL

gcloud config set project $PROJECT
gcloud config set compute/region europe-west1
gcloud config set compute/zone europe-west1-c


#static IP
gcloud compute addresses create web --region europe-west1
IP=`gcloud compute addresses describe web --region europe-west1 --format 'value(address)'`
echo $IP
echo Domain $IP.nip.io


#create cluster
gcloud container clusters create $CLUSTERNAME \
  --machine-type g1-small \
  --num-nodes 1
gcloud container clusters list
gcloud container clusters get-credentials $CLUSTERNAME


#install Helm
kubectl create serviceaccount tiller --namespace=kube-system
kubectl create clusterrolebinding tiller-admin --serviceaccount=kube-system:tiller --clusterrole=cluster-admin


#sleep 20s

helmins() {
 kubectl -n kube-system create serviceaccount tiller
 kubectl create clusterrolebinding tiller --clusterrole cluster-admin --serviceaccount=kube-system:tiller
 helm init --service-account=tiller
}
helmdel() {
 kubectl -n kube-system delete deployment tiller-deploy
 kubectl delete clusterrolebinding tiller
 kubectl -n kube-system delete serviceaccount tiller
}

helmins
helm repo update


#kubectl create clusterrolebinding cluster-admin-binding \
# --clusterrole cluster-admin \
# --user $(gcloud config get-value account)

#kubectl create serviceaccount --namespace kube-system tiller
#kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
#kubectl patch deploy --namespace kube-system tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'




helm upgrade \
 --install \
 --namespace kube-system \
 --set rbac.enabled=true \
 --set imageTag=1.7 \
 --set ssl.enabled=true \
 --set ssl.enforced=true \
 --set acme.enabled=true \
 --set acme.email=$EMAIL \
 --set acme.staging=false \
 --set acme.challengeType=tls-alpn-01 \
 --set acme.persistence.enabled=false \
 --set loadBalancerIP=$IP \
 traefik \
 stable/traefik

sleep 5

kubectl get svc traefik --namespace kube-system -w
kubectl describe svc traefik --namespace kube-system | grep Ingress | awk '{print $3}'


kubectl get svc
kubectl get certificate
kubectl get ingress


